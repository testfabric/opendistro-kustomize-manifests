# Example Overlay

This folder demonstrates a typical overlay for an Open Distro for Elasticsearch deployment.

## Resource Configuration

The Elasticsearch statefulset must be configured with your desired resource limits, memory, storage etc. This configuration can be found in the `patches/statefulset.yaml` file. The default values provided should be suitable for most small deployments.

## Public Key Infrastructure

The certificate issuer for use with `cert-manager` is required to be specified. This example includes a simple keypair based CA issuer. You may want to replace this with something more suitable for production environments such as Hashicorp Vault but this simple issuer should prove suitable for demonstration purposes. The issuer is configured in the `patches/certificate.yaml`, and `issuer.yaml` file.

### Certificate Authority Configuration

**!!! DO NOT USE THE DEMONSTRATION KEY PAIR IN YOUR DEPLOYMENT !!!**

If you want to use this demonstration configuration as a starting point, you are required, at a minimum, to generate your own certificate authority key pair. Instructions on how to generate your own key pair are available in [cert-manager's documentation](https://docs.cert-manager.io/en/latest/tasks/issuers/setup-ca.html).

