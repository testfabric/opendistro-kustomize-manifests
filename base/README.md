# Open Distro for Elasticsearch Base

This folder contains an Open Distro for Elasticsearch deployment demonstrating a typical use of the Open Distro for Elasticsearch security plugin.

This deployment consists of a simple three node cluster with all three nodes operating as shared data, and master nodes. This topology is not recommended by Elasticsearch for production workloads, however it is nonetheless widely used, and sufficient for this demonstration.

## Directory Structure

| Path | Description |
|------|-------------|
| `config/security/*` | Open Distro security configuration (eg. default users and auth methods) |
| `config/elasticsearch*.yaml` | Node configuration, split into a base config and individual node roles |
| `config/jvm.options` / `config/log4j2.properties` | JVM, and Open Distro logging preferences |
| `config/elasticsearch-init.sh` | Node initialization script, sets up keystores and configuration |            
| `config/elasticsearch-security-init.sh` | Security initialization script, sets up users etc, is run once |            

## Security Configuration

The security configuration for this deployment is found in the `config/security/` directory. This includes information about auth methods and a default set of internal users. A limited permissions healthcheck account is included in the default deployment.

## Runtime Patches

Due to the security initialization process, it is required to patch the Elasticsearch deployment at runtime after the security plugin has been initialized. These patches predominantly add liveliness and readiness probes.

Runtime patches are located in the `patches/` directory and are typically applied by the `elasticsearch-security-init` Job, and `elasticsearch-security-init.sh` script. Please avoid using runtime patches unless absolutely necessary. This is included only as a convenience feature.