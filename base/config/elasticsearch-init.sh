#!/bin/sh
set -e

#  ___         _             
# / __|_  _ __| |_ ___ _ __  
# \__ \ || (_-<  _/ -_) '  \ 
# |___/\_, /__/\__\___|_|_|_|
#      |__/                  

# We need procps for the sysctl command
yum install -y procps

# https://www.elastic.co/guide/en/elasticsearch/reference/current/vm-max-map-count.html
/sbin/sysctl -w 'vm.max_map_count=262144'

#   ___                   ___  _    _           
#  / _ \ _ __  ___ _ _   |   \(_)__| |_ _ _ ___ 
# | (_) | '_ \/ -_) ' \  | |) | (_-<  _| '_/ _ \
#  \___/| .__/\___|_||_| |___/|_/__/\__|_| \___/
#       |_|                                     

# Cleanup any existing configuration
rm -Rf /usr/share/elasticsearch/config/*

# Base configuration, eg JVM options et. al
cp '/conf/base/jvm.options' '/conf/base/log4j2.properties' \
  '/usr/share/elasticsearch/config/'

# Node specific configuration, merge all yamls, due to lack of config include mechanism
echo 'Merging elasticsearch configuration files ...'
find '/conf/shared' -maxdepth 1 -iregex '.*\.\(yaml\|yml\)$' \
  -exec sed -e '$a\' {} >> '/usr/share/elasticsearch/config/elasticsearch.yml' \;

#  ___      _    _ _      _  __         
# | _ \_  _| |__| (_)__  | |/ /___ _  _ 
# |  _/ || | '_ \ | / _| | ' </ -_) || |
# |_|  \_,_|_.__/_|_\__| |_|\_\___|\_, |
#                                  |__/ 

# We need openssl to convert into java key formats
yum install -y openssl

if [ ! -d '/usr/share/elasticsearch/config/certs' ]; then
  mkdir -p '/usr/share/elasticsearch/config/certs'
fi

# Shared trust store
echo 'Importing certificate authority into trust store ...'
keytool -import -v -trustcacerts -alias root -file /certs/api/ca.crt \
    -keystore /usr/share/elasticsearch/config/certs/cacerts.jks -storepass changeit -noprompt

# API key pair
echo 'Importing api key pair into key store ...'
openssl pkcs12 -export -password pass:changeit \
    -in /certs/api/tls.crt -inkey /certs/api/tls.key \
    -out api.p12 -name 'api' -CAfile /certs/api/ca.crt -caname 'api_root'
keytool -importkeystore -deststorepass changeit -destkeypass changeit \
    -destkeystore /usr/share/elasticsearch/config/certs/open-distro.jks -srckeystore api.p12 \
    -srcstoretype PKCS12 -srcstorepass changeit -alias 'api'

# Transport key pair
echo 'Importing transport key pair into key store ...'
openssl pkcs12 -export -password pass:changeit \
    -in /certs/transport/tls.crt -inkey /certs/transport/tls.key \
    -out transport.p12 -name 'transport' -CAfile /certs/transport/ca.crt -caname 'transport_root'
keytool -importkeystore -deststorepass changeit -destkeypass changeit \
    -destkeystore /usr/share/elasticsearch/config/certs/open-distro.jks -srckeystore transport.p12 \
    -srcstoretype PKCS12 -srcstorepass changeit -alias 'transport'