#!/bin/sh
set -e

HOST="$(ES_STATEFULSET_NAME)-0.$(ES_CLUSTER_NAME).$(ES_NAMESPACE).svc.cluster.local"
CACERT="/usr/share/elasticsearch/config/certs/ca.crt"
CERT="/usr/share/elasticsearch/config/certs/tls.crt"
KEY="/usr/share/elasticsearch/config/certs/tls.key"

# We need kubectl to patch the statefulset
curl -fsL 'https://storage.googleapis.com/kubernetes-release/release/v1.15.0/bin/linux/amd64/kubectl' \
 -o '/tmp/kubectl'
echo 'ecec7fe4ffa03018ff00f14e228442af5c2284e57771e4916b977c20ba4e5b39  /tmp/kubectl' | sha256sum -c -
chmod +x '/tmp/kubectl'

# We need openssl to convert into java key formats
yum install -y openssl

until curl -fsS --cacert "${CACERT}" --key "${KEY}" --cert "${CERT}" "https://${HOST}:9200/_cluster/health"
do
    sleep 5
done

# Setup any opendistro users
openssl pkcs8 -v1 "PBE-SHA1-3DES" -in "${KEY}" -topk8 -out "/tmp/tls-pkcs8.key" -nocrypt
chmod +x "/usr/share/elasticsearch/plugins/opendistro_security/tools/securityadmin.sh"
"/usr/share/elasticsearch/plugins/opendistro_security/tools/securityadmin.sh" \
  -cd "/usr/share/elasticsearch/config/security" \
  -cacert "${CACERT}" -cert "${CERT}" -key "/tmp/tls-pkcs8.key" \
  -h "${HOST}" -cn "$(ES_CLUSTER_NAME)"

# Now apply any runtime patches, eg healthchecks etc
for p in /patches/*.yaml
do
  /tmp/kubectl patch statefulset $(ES_STATEFULSET_NAME) -p "$(cat $p)"
done