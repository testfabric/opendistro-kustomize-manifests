# Open Distro Kustomize Manifests

Unofficial Kustomize manifests for deploying Open Distro for Elasticsearch. Intended for use in applications such as EFK logging stacks. Includes basic RBAC and mutual TLS configuration.

## Dependencies

* A Kubernetes cluster with RBAC enabled
* Jetstack [cert-manager](https://github.com/jetstack/cert-manager)
* An existing namespace, typically something like `kube-logging`

## Installation

The best method for using this set of manifests is to clone this repository and create your own overlay based on the included example. The example overlay is likely pretty close to what you will require. However you will be required to generate a new certificate authority key pair. Your deployment can then be kept up to date by merging in the base configuration from this repository.

## Trade Offs

* The nodes that make up the cluster share the same elasticsearch role and resource configuration.
* All nodes use the same TLS certificate for encrypting transport communications.